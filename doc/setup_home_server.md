# Ubuntu desktop on a laptop
## Disable suspending by closing lid
```
sudo -H gedit /etc/systemd/logind.conf
```
https://askubuntu.com/questions/15520/how-can-i-tell-ubuntu-to-do-nothing-when-i-close-my-laptop-lid

## Diable GUI
```
sudo systemctl set-default multi-user.target
```
https://askubuntu.com/questions/1056363/how-to-disable-gui-on-boot-in-18-04-bionic-beaver

## How to set fixed ip
https://linuxize.com/post/how-to-configure-static-ip-address-on-ubuntu-20-04/