# VPN server
https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04
![](2021-09-05-01-01-32.png)


```
kenji@dempek:~ $ cat ~/.ssh/id_rsa.pub | ssh kenji@145.239.80.178 'umask 0077; mkdir -p .ssh; cat >> .ssh/authorized_keys && echo "Key copied"'
kenji@145.239.80.178's password: 
```

## SSH setting
Not to use port 22
```
#Port 22
Port 2222
```

No password authentication
```
#PasswordAuthentication yes
PasswordAuthentication no
```

No root SSh access
```
#PermitRootLogin yes
PermitRootLogin no
```

Restart after change
```
kenji@vps528484:~$ sudo service ssh restart
```
This was needed
```
ufw allow 2222/tcp
```

## mgmt machine

## Check external IP
For leleuku480 user, set the following option
![](2021-09-06-20-55-17.png)
https://myaccount.google.com/lesssecureapps?pli=1&rapt=AEjHL4OCJQpKfkwVRXWij4705YPV4WcujBN5n82K3jSD8gVRBO1OjdS5UfBspzg5RjT08t00OY8F9JRWIf8AvKu3JhyifRxFSg