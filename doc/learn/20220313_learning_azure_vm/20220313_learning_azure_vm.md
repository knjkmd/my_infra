# What to learn
How to create, manage VMs on azure and understand the cost. 

# Azure compute services
There are 4 major Azure compute services
* Azure Virtual Machines (IaaS)
* Azure Container Instances (Paas)
* Azure App Service (PaaS)
* Azure Functions (or serverless computing)

https://docs.microsoft.com/en-us/learn/paths/az-900-describe-core-azure-services/


# Azure terms
Before start, it is important to understand some Azure terms as you need to set configure while you create a VM.

* **Resources**: Resources are instances of services that you create, like virtual machines, storage, or SQL databases.
* **Resource groups**: Resources are combined into resource groups, which act as a logical container into which Azure resources like web apps, databases, and storage accounts are deployed and managed.
* **Subscriptions**: A subscription groups together user accounts and the resources that have been created by those user accounts. For each subscription, there are limits or quotas on the amount of resources that you can create and use. Organizations can use subscriptions to manage costs and the resources that are created by users, teams, or projects.
* **Management groups**: These groups help you manage access, policy, and compliance for multiple subscriptions. All subscriptions in a management group automatically inherit the conditions applied to the management group.

![](2022-03-13-23-55-58.png)

# Tutorial

https://docs.microsoft.com/en-us/azure/virtual-machines/linux/quick-create-cli

* Create a resource group
```
az group create --name myResourceGroup --location eastus
```

* Create virtual machine
```
az vm create \
  --resource-group myResourceGroup \
  --name myVM \
  --image UbuntuLTS \
  --admin-username azureuser \
  --generate-ssh-keys
```

![](2022-03-14-00-20-18.png)

* Open port 80 for web traffic
```
az vm open-port --port 80 --resource-group myResourceGroup --name myVM
```

* Connect to virtual machine
```
ssh azureuser@20.228.210.21
```

* Install web server
```
sudo apt-get -y update
sudo apt-get -y install nginx
```

![](2022-03-14-00-26-28.png)

* Clean up resources
```
az group delete --name myResourceGroup
```

# Next Tutorial

* Get list of vm images
```
az vm image list --output table
```

* Check available VM sizes
```
az vm list-sizes --location eastus --output table
```

* Create VM with specific size
```
az vm create \
    --resource-group myResourceGroupVM \
    --name myVM3 \
    --image UbuntuLTS \
    --size Standard_F4s \
    --generate-ssh-keys
```

* Resize a VM
```
az vm show --resource-group myResourceGroupVM --name myVM --query hardwareProfile.vmSize
az vm list-vm-resize-options --resource-group myResourceGroupVM --name myVM --query [].name
az vm resize --resource-group myResourceGroupVM --name myVM --size Standard_DS4_v2
```

# VM power states
## Stopeed and deallocated are different!

![](2022-03-14-00-43-42.png)

* FInd the power state
```
az vm get-instance-view \
    --name myVM \
    --resource-group myResourceGroupVM \
    --query instanceView.statuses[1] --output table
```

# Management tasks
* Get IP address
```
az vm list-ip-addresses --resource-group myResourceGroupVM --name myVM --output table
```

* Stop virtual machine
```
az vm stop --resource-group myResourceGroupVM --name myVM
```

* Start virtual machine
```
az vm start --resource-group myResourceGroupVM --name myVM
```

* Deleting VM resources
> You can delete a VM, but by default this only deletes the VM resource, not the disks and networking resources the VM uses. You can change the default behavior to delete other resources when you delete the VM. For more information, see Delete a VM and attached resources.
Deleting a resource group also deletes all resources contained within, such as the VM, virtual network, and disk. The --no-wait parameter returns control to the prompt without waiting for the operation to complete. The --yes parameter confirms that you wish to delete the resources without an additional prompt to do so.

```
az group delete --name myResourceGroupVM --no-wait --yes
```