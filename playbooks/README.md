# my_infra

## Initial setup
* ssh to the machin as `root`
```
ssh root@machine_ip
```
* Create a new user
```
adduser username
```
* Add sudo permission
```
usermod -aG sudo username
```
* Set fire wall
```
ufw allow OpenSSH
ufw enable
```
* Put publick key. From the machine that has private key
```
ssh-copy-id username@machine_ip
```

* TODO: Disable root ssh access (Maybe in ansible)

* Change ssh port


## Run playbooks
To run a playbook, use following command.
```
ansible-playbook -i hosts.ini playbooks/playbook_name.yml   --ask-vault-pass --extra-vars '@passwd.yml'
```

* `hosts.ini` and `passwd.yml` are not part of the repository.

If you want to run specific tasks, tag the tasks you want to run and give `--tags <tag_name>`.


## System structure
![System diagram](./doc/image/system_diagram.png "System diagram")



## Reference
The initial setup
https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04


The most tasks are based on the tutorial in the link.
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04

The tutorial is missing the part of changing ownership of the unix socket folder. If you don't change it, you can only access main route `domain_name/` but not sub routes, e.g., `domain_name/subs`
https://stackoverflow.com/questions/39919053/django-gunicorn-sock-file-not-created-by-wsgi#_=_